var firebaseConfig = {
    apiKey: "AIzaSyA1wAGzexVyXt0VXUIa0KRx0Y42o2Iu3Rw",
    authDomain: "todo-list-pwa-91986.firebaseapp.com",
    databaseURL: "https://todo-list-pwa-91986.firebaseio.com",
    projectId: "todo-list-pwa-91986",
    storageBucket: "todo-list-pwa-91986.appspot.com",
    messagingSenderId: "887402402554",
    appId: "1:887402402554:web:5921042625c54a7c186f43",
    measurementId: "G-YCLL9MT4C8"
  };
// Initialize Firebase
firebase.initializeApp(firebaseConfig);

var url_string = window.location.href.toString();
var url = new URL(url_string);
var id = url.searchParams.get("id");
console.log(id);

var idb = firebase.firestore();

idb.collection('recipes').onSnapshot((snapshot) =>{
    console.log(snapshot);
    snapshot.docChanges().forEach(change => {
        if(change.type === "added" && change.doc.id === id){
          renderRecipe(change.doc.data(), change.doc.id);
        }
    })
})