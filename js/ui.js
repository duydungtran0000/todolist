const content = document.querySelector('.recipes');
const detail = document.querySelector('.detail');

document.addEventListener('DOMContentLoaded', function() {
  // nav menu
  const menus = document.querySelectorAll('.side-menu');
  M.Sidenav.init(menus, {edge: 'right'});
  // add recipe form
  const addForms = document.querySelectorAll('.side-form');
  M.Sidenav.init(addForms, {edge: 'left'});
});

const renderRecipe = (data, id) => {
  var status = "";
  if(data.status){
    status = "Tested"
  } else {
    status = "Not tested yet";
  }
  const html = `
    <div class="card-panel recipe white row" data-id="${id}">
      <img src="/img/dish.png" alt="recipe thumb" data-id="${id}">
      <div class="recipe-details">
        <div class="recipe-title">${data.title}</div>
        <div class="recipe-ingredients">${data.ingredients}</div>
        <div class="recipe-status">${status}</div>
      </div>
      <div class="recipe-delete">
        <i class="material-icons" data-id="${id}">delete_outline</i>
      </div>
    </div>
  `;
  content.innerHTML += html;
};

const removeRecipe = (id) => {
  const recipe = document.querySelector(`.recipe[data-id=${id}]`)
  recipe.remove();
}

